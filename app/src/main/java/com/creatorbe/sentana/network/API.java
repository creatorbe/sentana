package com.creatorbe.sentana.network;

import android.content.Context;

import com.creatorbe.sentana.util.Preferences;

import java.util.HashMap;

/**
 * Created by Sandy on 7/7/2015.
 */
public class API {
//    public static final String URL = "http://andtechnology.mobi/sentana/api";
//    public static final String URL = "http://idijakbar.org/sentana/api";
//    http://epaper.sentananews.com/api
//    public static final String URL = "http://epaper.sentananews.com/api";
    public static final String URL = "http://epaper.sentananews.com/admin/api";
    private static final String CLASS = "class";
    private static final String USER = "user";
    private static final String ACTION = "action";
    private static final String NEWSPAPER = "newspaper";
    public static final String USER_ID = "user_id";

    public static HashMap<String,String> login(String username,String password){
        HashMap<String, String> params=new HashMap<>();
        params.put(CLASS,USER);
        params.put(ACTION,"login");
        params.put("username",username);
        params.put("password",password);
        return params;
    }

    public static HashMap<String,String> register(Context context,String username,String password,String email,String phone){
        HashMap<String, String> params=getMaps(context,USER,"registration");
        params.put("username",username);
        params.put("password",password);
        params.put("email",email);
        params.put("phone",phone);
        return params;
    }

    public static HashMap<String, String> paperList(Context context,String editionId) {

        HashMap<String, String> params = getMaps(context, NEWSPAPER, "list");
        params.put("edition_id",editionId);
        return params;
    }

    private static HashMap<String, String> getMaps(Context context,String className,String action) {
        HashMap<String, String> params=new HashMap<>();
        params.put(CLASS,className);
        params.put(ACTION,action);
        if(Preferences.get(context,Preferences.PROFILE,API.USER_ID)!=null){
            params.put(API.USER_ID,Preferences.get(context,Preferences.PROFILE,API.USER_ID));
            params.put("token",Preferences.get(context,Preferences.PROFILE,"security_token"));
        }
        return params;
    }

    public static HashMap<String, String> editions(Context context) {

        return getMaps(context,NEWSPAPER,"editions");
    }

    public static HashMap<String, String> pages(Context context,String newspaper_id) {
        HashMap<String, String> params = getMaps(context, NEWSPAPER, "page_list");
        params.put("newspaper_id",newspaper_id);
        return params;
    }

    public static HashMap<String, String> articleList(Context context, String page_id) {
        HashMap<String, String> params = getMaps(context, NEWSPAPER, "article_list");
        params.put("page_id",page_id);
        return params;
    }

    public static HashMap<String, String> article(Context context, String article_id) {
        HashMap<String, String> params = getMaps(context, NEWSPAPER, "view_article");
        params.put("article_id",article_id);
        return params;
    }

    public static HashMap<String, String> subcribesList(Context context) {
        return getMaps(context,NEWSPAPER,"subscription_type");
    }

    public static HashMap<String, String> subscribe(Context context,String subscription_type) {
        HashMap<String, String> params = getMaps(context, USER, "subscribe");
        params.put("subscription_type",subscription_type);
        return params;
    }

    public static HashMap<String,String> timeRange(Context context) {
        return getMaps(context,NEWSPAPER,"time_range");
    }

    public static HashMap<String,String> searchList(Context context, String word_search,String edition_range,String time_range,int start,int end) {
        HashMap<String, String> params = getMaps(context, NEWSPAPER, "search");
        params.put("word_search",word_search);
        params.put("edition_range",edition_range);
        params.put("time_range",time_range);
        params.put("start", String.valueOf(start));
        params.put("end", String.valueOf(end));
        return params;
    }

    public static HashMap<String, String> favoriteList(Context context) {
        return getMaps(context,NEWSPAPER,"get_favorite");
    }

    public static HashMap<String, String> confirm(Context context) {
        HashMap<String, String> params = getMaps(context, USER, "confirm_payment");
        params.put("subscription_id",Preferences.get(context,Preferences.PROFILE,"subscription_id"));
        return params;
    }

    public static HashMap<String, String> favorite(Context context, String article_id) {
        HashMap<String, String> params = getMaps(context, NEWSPAPER, "do_favorite");
        params.put("article_id",article_id);
        return params;
    }

    public static HashMap<String, String> forgotPassword(Context context,String email) {
        HashMap<String, String> params = getMaps(context, USER, "forgot_password");
        params.put("email",email);
        return params;
    }

    public static HashMap<String, String> pageView(Context context, String page_id) {
        HashMap<String, String> params = getMaps(context, NEWSPAPER, "page_view");
        params.put("page_id",page_id);
        return params;
    }

    public static HashMap<String, String> memberProfile(Context context) {
        return getMaps(context, USER, "member_profile");
    }


    public static HashMap<String, String> qrCode(Context context,String qrCode) {
        HashMap<String, String> params = getMaps(context, USER, "claim_voucher");
        params.put("qr_data",qrCode);
        return params;
    }
}

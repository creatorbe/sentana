package com.creatorbe.sentana.network;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sandy on 7/7/2015.
 */
public class Network {

    public static String doPost(String urlString, HashMap<String, String> params) throws Exception {

        Log.i("Network API url",urlString);
        URL url = new URL(urlString);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type",
                "application/x-www-form-urlencoded");
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
//        urlConnection.setChunkedStreamingMode(0);

        OutputStream os = urlConnection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));


        String postDataString = getPostDataString(params);
        Log.i("Network params",postDataString);
        writer.write(postDataString);

        writer.flush();
        writer.close();
        os.close();
        int responseCode=urlConnection.getResponseCode();
        String response;
        if (responseCode == HttpsURLConnection.HTTP_OK) {
            response = readResponse(new InputStreamReader(urlConnection.getInputStream()));
        }
        else {
            throw new Exception(responseCode+"");
        }
        urlConnection.disconnect();
        return response;
    }

    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public static String readResponse(Reader stream) throws IOException {
        String response="";
        String line;
        BufferedReader br=new BufferedReader(stream);
        while ((line=br.readLine()) != null) {
            response+=line;
        }
        Log.i("Network API Result",response);
        return response;
    }

    public static byte[] readFile(File file) throws IOException {
        // Open file
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        } finally {
            f.close();
        }
    }

    public static String uploadObject(Context context, String postUrl, HashMap<String, String> files, HashMap<String, String> params) throws Exception {

        try {

            HttpClient httpClient 	= new DefaultHttpClient();
//            postUrl="http://192.168.0.3/uploadtest/uploadfile.php";
            HttpPost postRequest 	= new HttpPost(postUrl);
            Log.i("Network","Uploading " + postUrl);

//            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//            FileBody fileBody = new FileBody(image); //image should be a String
//            for (NameValuePair file : files) {
            for(Map.Entry<String, String> entry : files.entrySet()){
//                String key = entry.getKey();
//                Object value = entry.getValue();
                // ...
                File mfile = new File(entry.getValue());
//                byte[] bytes 	= FileUtils.readFileToByteArray(mfile);
                byte[] bytes 	= readFile(mfile);
                Log.i("Network", "Read " + String.valueOf(bytes.length) + " bytes");
                ByteArrayBody bab = new ByteArrayBody(bytes, mfile.getName());
                builder.addPart(entry.getKey(), bab);

            }

            for(Map.Entry<String, String> entry : params.entrySet()){
                builder.addTextBody(entry.getKey(), entry.getValue(), ContentType.TEXT_PLAIN);
                Log.i("Network params", entry.getKey() + ":" + entry.getValue());
            }

//
//            for(NameValuePair param :params){
//                reqEntity.addPart(params.get(x)[0], new StringBody(params.get(x)[1]));
//                builder.addTextBody(param.getName(),param.getValue(), ContentType.TEXT_PLAIN);
//            }
//            reqEntity.addPart("photo",bab);
            HttpEntity reqEntity = builder.build();
            postRequest.setEntity(reqEntity);

            HttpResponse result = httpClient.execute(postRequest);
            //return result.getEntity().getContent();
            try {
                InputStream inputStream = result.getEntity().getContent();
                int numRead = 0;
                byte[] data = new byte[512];
                StringBuffer sb = new StringBuffer();

                while ((numRead = inputStream.read(data)) > 0) {
                    sb.append(new String(data, 0, numRead));
                }
                inputStream.close();
                Log.i("Network Result", sb.toString());
                return sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }

//            if (is != null) {
//                return new JSONObject( API.readResponse(is) );
//
//            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
}

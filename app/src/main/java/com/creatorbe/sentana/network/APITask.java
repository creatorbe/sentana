package com.creatorbe.sentana.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import com.creatorbe.sentana.Sentana;
import com.creatorbe.sentana.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Sandy on 7/7/2015.
 */
public abstract class APITask extends AsyncTask<Void,Void,JSONObject> {

    private final Context context;
    private final HashMap<String, String> files;
    private HashMap<String, String> postParams;
    private Exception e;
    public APITask(Context context, HashMap<String, String> postParams,HashMap<String, String> files) {
        this.postParams = postParams;
        this.context=context;
        this.files=files;
    }
    public APITask(Context context, HashMap<String, String> postParams) {
        this(context,postParams,null);
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        try {
            if(files!=null){
                return new JSONObject(Network.uploadObject(context,API.URL,files,postParams));
            }
            return new JSONObject(Network.doPost(API.URL,postParams));
        } catch (Exception e) {
            this.e=e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        if(e!=null){
            onError(e);
        }else {
            try {
                if(jsonObject.has("err_no") && jsonObject.getInt("err_no")!=0)
                    onFailed(jsonObject);
                else {
                    if(jsonObject.has("user_status")){
                        String status = jsonObject.getString("user_status");
                        if(status.equals("pending_payment")){
                            onPendingPayment(jsonObject);
                        }else if(status.equals("pending_approval")){
                            onPendingApproval(jsonObject);
                        }else if(status.equals("subscribed")){
                            if(Preferences.get(context,Preferences.PROFILE,"subscribed_from")==null)
                                onSubcriptionInfo();
                        }else if(status.equals("unsubcribed")){
                            SharedPreferences.Editor editor = Preferences.get(context, Preferences.PROFILE).edit();
                            editor.remove("subscribed_from");
                            editor.remove("subscribed_to");
                            editor.apply();
                            onSubscriptionExpired();
                        }
                    }
                    onSuccess(jsonObject);
                }
            } catch (JSONException e1) {
                onError(e1);
            }
        }
    }

    protected void onSubscriptionExpired() {

    }

    private void onSubcriptionInfo() throws JSONException {
        new APITask(context, API.memberProfile(context)) {
            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
                HashMap<String, String> values=new HashMap<>();
                values.put("subscribed_from", jsonObject.getJSONObject("subscription_info").getString("subscribed_from"));
                values.put("subscribed_to", jsonObject.getJSONObject("subscription_info").getString("subscribed_to"));
                Preferences.puts(context, Preferences.PROFILE, values);
                privateOnSubscriptionAdded();
            }
        }.execute();

    }

    private void privateOnSubscriptionAdded(){
        onSubscriptionAdded();
    }

    protected void onSubscriptionAdded(){

    }

    protected void onPendingApproval(JSONObject jsonObject) {
        HashMap<String, String> values=new HashMap<>();
        values.put("user_status","pending_approval");
        Preferences.puts(context,Preferences.PROFILE,values);
    }

    protected void onPendingPayment(JSONObject jsonObject) {
        HashMap<String, String> values=new HashMap<>();
        values.put("user_status","pending_payment");
        Preferences.puts(context,Preferences.PROFILE,values);
    }

    protected void onFailed(JSONObject jsonObject) throws JSONException {
        new AlertDialog.Builder(context)
                .setMessage(jsonObject.getString("err_msg"))
                .create().show();
    }

    protected void onError(Exception e){
        Sentana.handleError(context,e);
    }

    protected abstract void onSuccess(JSONObject jsonObject) throws JSONException;
}

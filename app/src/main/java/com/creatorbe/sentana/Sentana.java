package com.creatorbe.sentana;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by Sandy on 7/7/2015.
 */
public class Sentana extends MultiDexApplication {
    public static void handleError(Context context,Exception e){
        e.printStackTrace();
    }

    public static ImageLoaderConfiguration getImageLoaderConfig(Context context) {
        DisplayImageOptions defaultImageOptions=new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageForEmptyUri(R.drawable.photo_default)
                .showImageOnFail(R.drawable.photo_default)
                .showImageOnLoading(R.drawable.loading_thumbnail)
                .build();
        return new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(defaultImageOptions)
                .build();
    }
}

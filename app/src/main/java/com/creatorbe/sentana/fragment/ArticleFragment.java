package com.creatorbe.sentana.fragment;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.creatorbe.sentana.MainActivity;
import com.creatorbe.sentana.R;
import com.creatorbe.sentana.Sentana;
import com.creatorbe.sentana.database.DBAdapter;
import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.util.Preferences;
import com.creatorbe.sentana.view.WebView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class ArticleFragment extends SentanaFragment {


    private FloatingActionButton btDownload;
    private JSONObject article;
    private TextView tvTitle;
    private TextView tvSubTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        btDownload = (FloatingActionButton) view.findViewById(R.id.btDownload);
        pb = view.findViewById(R.id.progressBar);
        final WebView wv = (WebView) view.findViewById(R.id.wvContent);
        wv.setFab(btDownload);
        wv.getSettings().setBuiltInZoomControls(true);
        try {
            if(Preferences.get(activity, Preferences.PROFILE, "subscribed_to")!=null) {
                Calendar today = Calendar.getInstance();
                today.set(Calendar.HOUR, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MILLISECOND, 0);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                Date date = dateFormat.parse(Preferences.get(activity, Preferences.PROFILE, "subscribed_to"));
                Calendar endSubsription = Calendar.getInstance();
                endSubsription.setTime(date);
                if (Preferences.get(activity, Preferences.PROFILE, "subscribed_to") != null){// && today.before(endSubsription)) {
                    view.findViewById(R.id.tvPleaseSubscribe).setVisibility(View.GONE);
                    view.findViewById(R.id.btDownload).setVisibility(View.VISIBLE);
                }
            }
        } catch (ParseException e) {
            Sentana.handleError(activity,e);
        }

        final Bundle arguments = getArguments();

        tvTitle = ((TextView) view.findViewById(R.id.tvTitle));
        tvSubTitle = ((TextView) view.findViewById(R.id.tvSubTitle));
        if(arguments.getString("title")!=null) {
            tvTitle.setText(arguments.getString("title"));
        }
        btDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                download();
            }
        });
        final TextView tvDate = (TextView) view.findViewById(R.id.tvDate);
        new MainActivity.MainAPITask(API.article(activity, arguments.getString("article_id")),activity) {
            @Override
            protected void onError(Exception e) {
                super.onError(e);
                removeLoad(0);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                addLoad(0);
            }

            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
                removeLoad(0);
                //android:text="Published May 7th, 2015"
//                SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy",Locale.getDefault());

                article = jsonObject.getJSONObject("article_data");
                tvTitle.setText(article.getString("title"));
                if(article.isNull("sub_title"))
                    tvSubTitle.setVisibility(View.GONE);
                else
                    tvSubTitle.setText(article.getString("sub_title"));
//                wv.loadData("<html><body>" + article.getString("content") + "</body></html>", "text/html", null);
                wv.loadData(article.getString("content"), "text/html; charset=UTF-8", null);
                btDownload.setEnabled(true);
                tvDate.setText(article.getString("news_date"));

            }
        }.execute();


        view.findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
        return view;
    }

    private void download() {
        new MainActivity.MainAPITask(API.favorite(activity,getArguments().getString("article_id")), activity){
            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {

                removeLoad(1);
                new AlertDialog.Builder(activity).setMessage(jsonObject.getString("msg")).create().show();
                btDownload.setVisibility(View.GONE);
                DBAdapter dbAdapter = DBAdapter.getInstance(activity);
                dbAdapter.open();
                ContentValues values=new ContentValues();
                values.put("title", article.getString("title"));
                values.put("category_id", article.getString("category_id"));
                values.put("category_name", article.getString("category_name"));
                values.put("keywords", article.getString("keywords"));
                values.put("content", article.getString("content"));
                values.put("news_date", article.getString("news_date"));
                Cursor cursor = dbAdapter.query("article",new String[]{"_id"},"article_id=?",new String[]{article.getString("article_id")},null,null,null);

                if(cursor.getCount()>0){
                    dbAdapter.update("favorite",values,"article_id=?", article.getString("article_id"));
                }else {
                    values.put("article_id", article.getString("article_id"));
                    dbAdapter.insert("favorite", null, values);
                }
                dbAdapter.close();
            }
            @Override
            protected void onError(Exception e) {
                super.onError(e);
                removeLoad(1);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                addLoad(1);
            }
        }.execute();

    }


}

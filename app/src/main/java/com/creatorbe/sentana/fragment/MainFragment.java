package com.creatorbe.sentana.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.creatorbe.sentana.MainActivity;
import com.creatorbe.sentana.R;
import com.creatorbe.sentana.Sentana;
import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.util.Preferences;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainFragment extends SentanaFragment {
    MainFragmentListener listener;
    private TextView tvEdition;
    private JSONArray edition;
    private JSONArray papers=new JSONArray();
    private PapersAdapter mAdapter;
    private boolean isSearch=false;
//    private String search;
//    private String timeRange;
//    private int end=40;
//    private int start=0; //todo paging not yet implemented
    private MainActivity mainActivity;

    public interface MainFragmentListener{
        void onPaperClick(String news_id);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.lvPapers);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new PapersAdapter();
        mRecyclerView.setAdapter(mAdapter);
//        HashMap<String, String> params=new HashMap<>();
        pb = view.findViewById(R.id.progressBar);
        tvEdition = (TextView)view.findViewById(R.id.tvEdition);
        tvEdition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditions();
            }
        });
        Bundle args = getArguments();
        if(args!=null && args.containsKey("word_search")){
            isSearch = true;
//            search = args.getString("word_search");
//            timeRange = args.getString("time_range");
        }
        try {

            String jsonString = Preferences.get(activity, Preferences.SETTING, "editions");
            if(jsonString!=null) {
                edition = new JSONArray(jsonString);
                if (edition.length() > 0) {
//                    if(isSearch){
//                        tvEdition.setEnabled(true);
//                        tvEdition.setTag(args.getString("edition_range"));
//                        tvEdition.setText(args.getString("edition"));
//                        new PaperTask(activity, API.searchList(activity,
//                                search,args.getString("edition_range"),timeRange,String.valueOf(start),
//                                String.valueOf(end)), (MainActivity) activity).execute();
//                    }else {
                        setEdition(0);
//                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new MainActivity.MainAPITask(API.editions(activity), activity){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                addLoad(0);
            }

            @Override
            protected void onError(Exception e) {
                super.onError(e);
                removeLoad(0);
            }

            @Override
            protected void onSuccess(JSONObject jsonObject) {
//                mAdapter.notifyDataSetChanged();
                removeLoad(0);
                try {
                    HashMap<String, String> values = new HashMap<>();

                    edition = jsonObject.getJSONArray("data");
                    if(edition.length()>0) {
                        values.put("editions", edition.toString());
                        Preferences.puts(activity, Preferences.SETTING, values);
                        if(tvEdition.getTag()==null)setEdition(0);
                    }

                }catch (Exception e){
                    Sentana.handleError(activity,e);
                }
            }
        }.execute();
        return view;
    }

    private class PaperTask extends MainActivity.MainAPITask{

        public PaperTask(HashMap<String, String> postParams,MainActivity activity) {
            super(postParams,activity);
        }

        @Override
        protected void onError(Exception e) {
            super.onError(e);
            removeLoad(1);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            addLoad(1);
        }

        @Override
        protected void onSuccess(JSONObject jsonObject) {
            removeLoad(1);
            try {
                if(isSearch)
                    papers=jsonObject.getJSONArray("data");
                else
                    papers=jsonObject.getJSONArray("newspaper_list");
                mAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                Sentana.handleError(activity, e);
            }

        }
    }


    private void setEdition(int i) throws JSONException {
        tvEdition.setEnabled(true);
        tvEdition.setTag(edition.getJSONObject(i).getString("edition_id"));
        tvEdition.setText(edition.getJSONObject(i).getString("edition_name"));
//        if(isSearch)
//            new PaperTask(activity, API.searchList(activity,
//                    search,edition.getJSONObject(i).getString("edition_id"),
//                    timeRange,String.valueOf(start),String.valueOf(end)), (MainActivity) activity).execute();
//        else
            new PaperTask(API.paperList(activity,edition.getJSONObject(i).getString("edition_id")),activity).execute();
    }

    private void showEditions() {
        EditionAdapter editionAdapter=new EditionAdapter();
        new AlertDialog.Builder(activity)
                .setTitle("Editions")
                .setAdapter(editionAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            setEdition(which);
                        } catch (JSONException e) {
                            Sentana.handleError(activity,e);
                        }
                    }
                })
                .create().show();
    }

    private class PapersAdapter extends RecyclerView.Adapter<ViewHolder>{
        private final ImageLoader imageLoader;
//        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE d/M/yyyy", Locale.getDefault());

        public PapersAdapter(){
            imageLoader=ImageLoader.getInstance();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(activity)
                    .inflate(R.layout.list_item_papers, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
//            c.add(Calendar.DAY_OF_MONTH,0-i);
//            Calendar c = Calendar.getInstance();
            try {
                final JSONObject object = papers.getJSONObject(i);
                viewHolder.date.setText(object.getString("news_date"));
                if(!imageLoader.isInited()) imageLoader.init(Sentana.getImageLoaderConfig(activity));
                imageLoader.displayImage(object.getString("news_page"), viewHolder.image);
                viewHolder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            listener.onPaperClick(object.getString("news_id"));
                        } catch (JSONException e) {
                            Sentana.handleError(activity,e);
                        }
                    }
                });
            } catch (JSONException e) {
                Sentana.handleError(activity, e);
            }

        }

        @Override
        public int getItemCount() {
            return papers.length();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView date;
        public View view;
        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.wvContent);
            date = (TextView) itemView.findViewById(R.id.tvDate);
            view = itemView;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener= (MainFragmentListener) activity;
        mainActivity = (MainActivity) activity;
    }

    private class EditionAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return edition.length();
        }

        @Override
        public Object getItem(int position) {
            try {
                return edition.getJSONObject(position);
            } catch (JSONException e) {
                return null;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null){
                convertView = LayoutInflater.from(activity).inflate(android.R.layout.simple_list_item_1,parent,false);

            }
            try {
                ((TextView)convertView.findViewById(android.R.id.text1)).setText(edition.getJSONObject(position).getString("edition_name"));
            } catch (JSONException e) {
                Sentana.handleError(activity,e);
            }
            return convertView;
        }
    }


}

package com.creatorbe.sentana.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.creatorbe.sentana.MainActivity;
import com.creatorbe.sentana.R;
import com.creatorbe.sentana.Sentana;
import com.creatorbe.sentana.SubscribeActivity;
import com.creatorbe.sentana.network.API;
//import com.creatorbe.sentana.network.APITask;
import com.creatorbe.sentana.util.Preferences;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends SentanaFragment {

    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_GALLERY = 1;
    private static final int REQUEST_CROP = 2;
    private EditText etEmail,etPhone,etPassword,etConfirm;
    private CheckBox cbNew,cbPromo;
    private String password="";
    private ImageView ivPhoto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        view.findViewById(R.id.btLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onLogout();
            }
        });
        view.findViewById(R.id.btSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        view.findViewById(R.id.btConfirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm();
            }
        });
        view.findViewById(R.id.btRenew).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                renew();
            }
        });
        String subscribed_to = Preferences.get(activity, Preferences.PROFILE, "subscribed_to");
        if(subscribed_to!=null)((TextView)view.findViewById(R.id.tvPackage)).setText(getString(R.string.you_subscribe_package_until) + " " + subscribed_to);
        SharedPreferences pref = Preferences.get(activity, Preferences.PROFILE);
        etEmail = (EditText)view.findViewById(R.id.etEmail);
        etPhone = (EditText)view.findViewById(R.id.etPhone);
        etPassword = (EditText)view.findViewById(R.id.etPassword);
        etConfirm = (EditText)view.findViewById(R.id.etConfirmPassword);
        cbNew = (CheckBox)view.findViewById(R.id.cbNewAlert);
        cbPromo = (CheckBox)view.findViewById(R.id.cbPromoAlert);
        etEmail.setText(pref.getString("user_email",""));
        etPhone.setText(pref.getString("phone", ""));
        cbNew.setChecked(pref.getString("new_edition_notif", "off").equals("on"));
        cbPromo.setChecked(pref.getString("promo_notif","off").equals("on"));
        originalFile=new File(activity.getExternalFilesDir(null) + "/profile.jpg");
        ImageLoader imageLoader = ImageLoader.getInstance();
        if(!imageLoader.isInited()) imageLoader.init(Sentana.getImageLoaderConfig(activity));
        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
        imageLoader.displayImage(Preferences.get(activity, Preferences.PROFILE, "user_photo"), ivPhoto);
        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new android.app.AlertDialog.Builder(activity)
                        .setMessage(getString(R.string.take_photo_from))
                        .setPositiveButton(getString(R.string.camera), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                camera(activity, "/ktp_camera.jpg", REQUEST_CAMERA);
                            }
                        })
                        .setNeutralButton(getString(R.string.gallery), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                gallery(REQUEST_GALLERY);
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), null)
                        .create().show();

//                Crop.pickImage(activity);
            }
        });
        return view;
    }

    public static void camera(Activity act, String filePath, int request){
        Intent cropIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File cameraFile=new File(act.getExternalFilesDir(null) + filePath);
        Uri outputFileUri = Uri.fromFile(cameraFile);
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
//        cropIntent.putExtra("crop", "true");
//        cropIntent.putExtra("aspectX", 1);
//        cropIntent.putExtra("aspectY", 1);
//        cropIntent.putExtra("scale", true);
//        cropIntent.putExtra("outputX", 500);
//        cropIntent.putExtra("outputY", 500);
        cropIntent.putExtra("return-data", false);
        cropIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        act.startActivityForResult(cropIntent, request);
    }

    private File originalFile;
    private void gallery(int request){

        Uri outputFileUri = Uri.fromFile(originalFile);
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra("crop", "true");
		photoPickerIntent.putExtra("aspectX", 1);
		photoPickerIntent.putExtra("aspectY", 1);
		photoPickerIntent.putExtra("scale", true);
//		photoPickerIntent.putExtra("outputX", 500);
//		photoPickerIntent.putExtra("outputY", 500);
        photoPickerIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        photoPickerIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(photoPickerIntent, request);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==Activity.RESULT_OK){
            switch(requestCode){
                case REQUEST_CAMERA:
                    File cameraFile=new File(activity.getExternalFilesDir(null) + "/profile.jpg");
                    Uri cameraFileUri = Uri.fromFile(cameraFile);
//                    Uri outputFileUri = Uri.fromFile(originalFile);

//                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    Intent photoPickerIntent = new Intent("com.android.camera.action.CROP");
                    photoPickerIntent.setDataAndType(cameraFileUri,"image/*");
//                    photoPickerIntent.setType("image/*");
//                    photoPickerIntent.putExtra("crop", "true");

                    photoPickerIntent.putExtra("aspectX", 1);
                    photoPickerIntent.putExtra("aspectY", 1);
                    photoPickerIntent.putExtra("scale", true);

                    photoPickerIntent.putExtra("return-data", false);
                    Uri outputFileUri = Uri.fromFile(originalFile);
                    photoPickerIntent.putExtra(MediaStore.EXTRA_OUTPUT,outputFileUri);
                    photoPickerIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
                    startActivityForResult(photoPickerIntent, REQUEST_CROP);
                    break;
                case REQUEST_CROP:
                case REQUEST_GALLERY:
                    setBitmapPhoto();
                    break;

            }
        }else{
            if(requestCode==REQUEST_CROP) setBitmapPhoto();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setBitmapPhoto() {
        originalFile=new File(activity.getExternalFilesDir(null) + "/profile.jpg");
        if(originalFile.exists()) {
            ivPhoto.setImageBitmap(BitmapFactory.decodeFile(activity.getExternalFilesDir(null) + "/profile.jpg"));
        }
    }

    private void renew() {
        startActivity(new Intent(activity, SubscribeActivity.class));
    }

    private void confirm() {
        new MainActivity.MainAPITask(API.confirm(activity), activity){
            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
                pd.dismiss();
                new AlertDialog.Builder(activity).setMessage(jsonObject.getString("msg"))
                        .setPositiveButton(R.string.ok,null).create().show();
            }

            ProgressDialog pd;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd=new ProgressDialog(activity);
                pd.setMessage(getString(R.string.please_wait));
                pd.setCancelable(false);
                pd.show();
            }
        }.execute();
    }

    private void saveToApi(){
        final HashMap<String, String> params=new HashMap<>();

        HashMap<String, String> files=new HashMap<>();

        params.put("class", "user");
        params.put("action", "update_profile");
        params.put("user_id", Preferences.get(activity, Preferences.PROFILE, API.USER_ID));
        params.put("token", Preferences.get(activity, Preferences.PROFILE, "security_token"));
        params.put("phone", etPhone.getText().toString());
        params.put("email", etEmail.getText().toString());
        params.put("new_edition_notif", cbNew.isChecked() ? "on" : "off");
        params.put("promo_notif", cbPromo.isChecked() ? "on" : "off");
        params.put("old_password", password);
        params.put("new_password",etPassword.getText().toString());
        if(new File(activity.getExternalFilesDir(null) + "/profile.jpg").exists())
            files.put("photo",activity.getExternalFilesDir(null) + "/profile.jpg");

        new MainActivity.MainAPITask(params,files, activity)
        {
            ProgressDialog pd;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = new ProgressDialog(activity);
                pd.setMessage(getString(R.string.please_wait));
                pd.setCancelable(false);
                pd.show();
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                //super.onPostExecute(jsonObject);
                pd.dismiss();
                try {
                    //JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject.getInt("err_no")==0){
                        HashMap<String, String> params=new HashMap<>();
                        params.put("phone", etPhone.getText().toString());
                        params.put("email", etEmail.getText().toString());
                        params.put("new_edition_notif", cbNew.isChecked() ? "on" : "off");
                        params.put("promo_notif", cbPromo.isChecked() ? "on" : "off");
                        params.put("password", password);
                        Preferences.puts(activity,Preferences.PROFILE,params);

                        new AlertDialog.Builder(activity)
                                .setMessage(jsonObject.getString("msg"))
                                .setPositiveButton(R.string.ok, null)
                                .create().show();
                    }else if(jsonObject.getInt("err_no")==498){
                        listener.onLogout();
                    }else{
                        new AlertDialog.Builder(activity)
                                .setMessage(jsonObject.getString("err_msg"))
                                .setPositiveButton(R.string.ok, null)
                                .create().show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {

            }
        }.execute();
    }

    private void save() {
        if(etPassword.getText().length()>0){
            if(!etPassword.getText().toString().equals(etConfirm.getText().toString())){
                etConfirm.setError(getString(R.string.password_not_equal));
            }else{
                final EditText etPass = new EditText(activity);
                etPass.setInputType(etPass.getInputType()+ InputType.TYPE_TEXT_VARIATION_PASSWORD);
                new AlertDialog.Builder(activity)
                        .setTitle(R.string.old_password)
                        .setView(etPass)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                password = etPass.getText().toString();
                                saveToApi();
                            }
                        })
                        .setCancelable(false)
                        .create().show();
            }
        }else{
            saveToApi();
        }

        
//        new APITask(activity, API.updateProfile(activity,etEmail.getText().toString(),
//                etPhone.getText().toString(),etPassword.getText().toString(),password,
//                cbNew.isChecked()?"on":"off",
//                cbPromo.isChecked()?"on":"off")){}.execute();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (ProfileListener) activity;
    }

    private ProfileListener listener;
    public interface ProfileListener{
        void onLogout();
    }
}

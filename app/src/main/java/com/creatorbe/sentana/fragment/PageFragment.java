package com.creatorbe.sentana.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.creatorbe.sentana.MainActivity;
import com.creatorbe.sentana.R;
import com.creatorbe.sentana.network.API;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class PageFragment extends SentanaFragment {

    private static final int LOAD_CONTENT = 2;


    private WebView wvContent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        pb = view.findViewById(R.id.progressBar);
        wvContent = (WebView)view.findViewById(R.id.wvContent);
        WebSettings webSettings = wvContent.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);
        wvContent.addJavascriptInterface(new WebAppInterface(activity), "Android");

        String pageId = getArguments().getString("page_id");
        loadContent(pageId);
        return view;

    }

    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void article_id(String article_id) {
            ((PagesFragment)getParentFragment()).listener.onSeeArticle(article_id,null);
        }
    }



    private void loadContent(String page_id) {

        new MainActivity.MainAPITask(API.pageView(activity, page_id),activity){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                addLoad(LOAD_CONTENT);
            }

            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
                removeLoad(LOAD_CONTENT);
                if(!jsonObject.isNull("imagemap")) {
                    wvContent.loadData("<center>" + jsonObject.getString("imagemap") + "</center>","text/html","UTF-8");
                }

            }
        }.execute();

    }
}

package com.creatorbe.sentana.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.creatorbe.sentana.MainActivity;
import com.creatorbe.sentana.R;
import com.creatorbe.sentana.Sentana;
import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.view.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends SentanaFragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.lvFavorite);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL_LIST));
        pb=view.findViewById(R.id.progressBar);
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        final FavoriteAdapter mAdapter = new FavoriteAdapter();
        mRecyclerView.setAdapter(mAdapter);

        String word_search=getArguments().getString("word_search");

        String edition_range=getArguments().getString("edition_range");
        String time_range=getArguments().getString("time_range");
        int start=0;
        int end=40;
        new MainActivity.MainAPITask(API.searchList(activity,word_search,edition_range,time_range,start,end), activity){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pb.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                pb.setVisibility(View.GONE);
                super.onPostExecute(jsonObject);

            }

            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
                data = jsonObject.getJSONArray("data");
            }
        }.execute();
        return view;
    }
    JSONArray data = new JSONArray();
    private class FavoriteAdapter extends RecyclerView.Adapter<ViewHolder>{

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(activity)
                    .inflate(R.layout.list_item_search, viewGroup, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {

            try {
                JSONObject jsonObject = data.getJSONObject(i);
                viewHolder.tvTitle.setText(jsonObject.getString("title"));
                viewHolder.view.setTag(jsonObject.getString("article_id"));
                viewHolder.tvContent.setText(Html.fromHtml(jsonObject.getString("preview")));
            } catch (JSONException e) {
                Sentana.handleError(activity,e);
            }

        }

        @Override
        public int getItemCount() {

            return data.length();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle,tvContent;
        public View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view=itemView;
            tvTitle= (TextView)view.findViewById(R.id.tvTitle);
            tvContent= (TextView)view.findViewById(R.id.tvContent);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSeeArticle(v.getTag().toString(),tvTitle.getText().toString());
                }
            });

        }
    }

    PagesFragment.PreviewArticleListener listener;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (PagesFragment.PreviewArticleListener) activity;
    }
}

package com.creatorbe.sentana.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creatorbe.sentana.MainActivity;
import com.creatorbe.sentana.R;
import com.creatorbe.sentana.Sentana;
import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.network.APITask;
import com.creatorbe.sentana.util.Preferences;
import com.creatorbe.sentana.view.DividerItemDecoration;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class PagesFragment extends SentanaFragment {



//    private View tvPages,tvArticle,vPages,vArticle;
    private View vPages,vArticle;
    private RecyclerView rvArticles,rvPages;
    private ArticlesAdapter mArticleAdapter;
    private PagesAdapter mPagesAdapter;
    private final static int LOAD_PAGES=0;
    private final static int LOAD_ARTICLES=1;
    private JSONArray pages=new JSONArray();
    private ViewPager viewPager;

    private JSONArray articles=new JSONArray();
    private PagerAdapter pagerAdapter;


    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_pages, container, false);
        imageLoader=ImageLoader.getInstance();
//        tvPages = view.findViewById(R.id.tvPages);
//        tvArticle = view.findViewById(R.id.tvArticle);
        vPages = view.findViewById(R.id.layoutPages);
        vArticle = view.findViewById(R.id.layoutArticle);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        pagerAdapter = new PagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    new LoadArticleTask(pages.getJSONObject(position).getString("page_id")).execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pb = view.findViewById(R.id.progressBar);
        rvArticles = (RecyclerView) view.findViewById(R.id.rvArticles);
        rvPages = (RecyclerView) view.findViewById(R.id.rvPages);



        if(Preferences.get(activity,Preferences.PROFILE,"subscription_id")!=null){
            view.findViewById(R.id.tvPleaseSubscribe).setVisibility(View.GONE);
        }

        view.findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });

//        setEvents();
        String newspaper_id = getArguments().getString("newspaper_id");

        prepareList();

        if(pages.length()==0) {
            new MainActivity.MainAPITask(API.pages(activity, newspaper_id),activity) {
                @Override
                protected void onError(Exception e) {
                    super.onError(e);
                    removeLoad(LOAD_PAGES);
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    addLoad(LOAD_PAGES);
                }

                @Override
                protected void onSuccess(JSONObject jsonObject) throws JSONException {
                    removeLoad(LOAD_PAGES);
                    pages = jsonObject.getJSONArray("page_list");
                    mPagesAdapter.notifyDataSetChanged();
                    if (pages.length() > 0) {
                        preparePageView();
                    }
                }
            }.execute();
        }else{
            preparePageView();
        }
        return view;
    }

    private void preparePageView() {
        pagerAdapter.notifyDataSetChanged();
        if(pages.length()>0) {
            try {
                JSONObject object = pages.getJSONObject(0);
                new LoadArticleTask(object.getString("page_id")).execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private class LoadArticleTask extends APITask{

        public LoadArticleTask(String page_id) {
            super(activity,API.articleList(activity,page_id));
        }

        @Override
        protected void onError(Exception e) {
            super.onError(e);
            removeLoad(LOAD_ARTICLES);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            addLoad(LOAD_ARTICLES);
        }

        @Override
        protected void onSuccess(JSONObject jsonObject) throws JSONException {
            removeLoad(LOAD_ARTICLES);
            if(!jsonObject.isNull("article_list")) {
                articles = jsonObject.getJSONArray("article_list");
                mArticleAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void removeLoad(int removeIndexLoad) {
        indexLoad.remove(removeIndexLoad);
        if(indexLoad.size()==0){
            pb.setVisibility(View.INVISIBLE);
        }
    }

    private void prepareList() {

        rvArticles.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvArticles.setLayoutManager(mLayoutManager);
        rvArticles.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL_LIST));
        mArticleAdapter = new ArticlesAdapter();
        rvArticles.setAdapter(mArticleAdapter);

        rvPages.setHasFixedSize(true);
        LinearLayoutManager mPagesLayoutManager = new LinearLayoutManager(activity);
        mPagesLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvPages.setLayoutManager(mPagesLayoutManager);
        mPagesAdapter = new PagesAdapter();
        rvPages.setAdapter(mPagesAdapter);
    }

//    private void setEvents() {
//
//        tvPages.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!tvPages.isActivated()) {
//                    Animation bottomUp = AnimationUtils.loadAnimation(activity,
//                            R.anim.bottom_up);
//                    tvPages.setActivated(true);
//                    tvArticle.setActivated(false);
//                    vPages.bringToFront();
//                    vPages.startAnimation(bottomUp);
//                    vPages.setVisibility(View.VISIBLE);
//                    vArticle.setVisibility(View.GONE);
//                }
//            }
//        });
//
//        tvArticle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!tvArticle.isActivated()) {
//                    Animation bottomUp = AnimationUtils.loadAnimation(activity,
//                            R.anim.bottom_up);
//                    tvPages.setActivated(false);
//                    tvArticle.setActivated(true);
//                    vArticle.bringToFront();
//                    vArticle.startAnimation(bottomUp);
//                    vArticle.setVisibility(View.VISIBLE);
//                    vPages.setVisibility(View.GONE);
//
//                }
//            }
//        });
//    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener= (PreviewArticleListener) activity;
    }

    private class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder>{
//        private final ImageLoader imageLoader;
//        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE d/M/yyyy", Locale.getDefault());

//        public ArticlesAdapter(){
//            imageLoader=ImageLoader.getInstance();
//        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(activity)
                    .inflate(R.layout.list_item_articles, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
//            c.add(Calendar.DAY_OF_MONTH,0-i);
//            Calendar c = Calendar.getInstance();
            try {
                final JSONObject object = articles.getJSONObject(i);
                viewHolder.title.setText(object.getString("title"));
//                if(!imageLoader.isInited()) imageLoader.init(Sentana.getImageLoaderConfig(activity));
//                imageLoader.displayImage(object.getString("news_page"), viewHolder.image);
                viewHolder.title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            listener.onSeeArticle(object.getString("article_id"),object.getString("title"));
                        } catch (JSONException e) {
                            Sentana.handleError(activity,e);
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return articles.length();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView title;
            public ViewHolder(View itemView) {
                super(itemView);

                title = (TextView) itemView;
            }
        }
    }
    private ImageLoader imageLoader;
    private class PagesAdapter extends RecyclerView.Adapter<PagesAdapter.ViewHolder>{

//        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE d/M/yyyy", Locale.getDefault());

        public PagesAdapter(){

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(activity)
                    .inflate(R.layout.list_item_page, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, int i) {

            try {
                final JSONObject object = pages.getJSONObject(i);
                viewHolder.page.setText(getString(R.string.page) + " " + object.getString("page_num"));
                if(!imageLoader.isInited()) imageLoader.init(Sentana.getImageLoaderConfig(activity));
                imageLoader.displayImage(object.getString("page_file"), viewHolder.image);
                viewHolder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                            tvPages.setActivated(false);
                            vPages.setVisibility(View.GONE);

                            setViewPagerPage(viewHolder.getAdapterPosition());


                    }
                });
            } catch (JSONException e) {
                Sentana.handleError(activity,e);
            }

        }

        @Override
        public int getItemCount() {
            return pages.length();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView image;
            public TextView page;
            public View view;
            public ViewHolder(View itemView) {
                super(itemView);
                image = (ImageView) itemView.findViewById(R.id.wvContent);
                page = (TextView) itemView.findViewById(R.id.tvTitle);
                view = itemView;
            }
        }
    }

    private void setViewPagerPage(int adapterPosition) {
        viewPager.setCurrentItem(adapterPosition);
    }

    private class PagerAdapter extends FragmentPagerAdapter {
        public PagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            PageFragment pageFragment = new PageFragment();
            Bundle args = new Bundle();
            try {
                args.putString("page_id",pages.getJSONObject(position).getString("page_id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pageFragment.setArguments(args);
            return pageFragment;
        }

        @Override
        public int getCount() {
            return pages.length();
        }
    }

    public PreviewArticleListener listener;
    public interface PreviewArticleListener{
        void onSeeArticle(String article_id, String title);
    }
}

package com.creatorbe.sentana.fragment;


import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.creatorbe.sentana.MainActivity;
import com.creatorbe.sentana.R;
import com.creatorbe.sentana.database.DBAdapter;
import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.view.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends SentanaFragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.lvFavorite);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL_LIST));
        pb=view.findViewById(R.id.progressBar);
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        final FavoriteAdapter mAdapter = new FavoriteAdapter();
        mRecyclerView.setAdapter(mAdapter);
        new MainActivity.MainAPITask(API.favoriteList(activity),activity){
            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
                pb.setVisibility(View.GONE);
                if(!jsonObject.isNull("favourite_list")) {
                    DBAdapter dbAdapter = DBAdapter.getInstance(activity);
                    dbAdapter.open();
                    JSONArray articles = jsonObject.getJSONArray("favourite_list");
                    for (int x = 0; x < articles.length(); x++) {
                        JSONObject article = articles.getJSONObject(x);

                        ContentValues values = new ContentValues();
                        if(article.has("title"))values.put("title", article.getString("title"));
//                        values.put("category_id", article.getString("category_id"));
//                        values.put("category_name", article.getString("category_name"));
//                        values.put("keywords", article.getString("keywords"));
                        if(article.has("preview"))values.put("preview", article.getString("preview"));
                        if(article.has("news_date"))values.put("news_date", article.getString("news_date"));
                        Cursor cursor = dbAdapter.query("article", new String[]{"_id"}, "article_id=?", new String[]{article.getString("article_id")}, null, null, null);

                        if (cursor.getCount() > 0) {
                            dbAdapter.update("article", values, "article_id=?", article.getString("article_id"));
                        } else {
                            values.put("article_id", article.getString("article_id"));
                            dbAdapter.insert("article", null, values);
                        }

                    }
                    favCursor = dbAdapter.query("article",null,null,null,null,null,null);

                    mAdapter.notifyDataSetChanged();
                }
            }
        }.execute();
        return view;
    }

    private Cursor favCursor;
    private class FavoriteAdapter extends RecyclerView.Adapter<ViewHolder>{

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(activity)
                    .inflate(R.layout.list_item_favorite, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(v);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            favCursor.moveToPosition(i);
            viewHolder.tvTitle.setText(favCursor.getString(favCursor.getColumnIndex("title")));
            viewHolder.tvDate.setText(favCursor.getString(favCursor.getColumnIndex("news_date")));
            viewHolder.tvContent.setText(Html.fromHtml(favCursor.getString(favCursor.getColumnIndex("preview"))));
            viewHolder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onFavClick(favCursor.getString(favCursor.getColumnIndex("article_id")),favCursor.getString(favCursor.getColumnIndex("title")));
                }
            });
        }

        @Override
        public int getItemCount() {
            return favCursor ==null?0: favCursor.getCount();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public TextView tvTitle,tvDate,tvContent;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvTitle= (TextView)view.findViewById(R.id.tvTitle);
            tvDate= (TextView)view.findViewById(R.id.tvDate);
            tvContent= (TextView)view.findViewById(R.id.tvContent);
        }
    }

    FavoriteFragmentListener listener;
    public interface FavoriteFragmentListener {
        void onFavClick(String article_id,String title);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener= (FavoriteFragmentListener) activity;
    }
}

package com.creatorbe.sentana.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creatorbe.sentana.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShareFragment extends SentanaFragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_share, container, false);
        view.findViewById(R.id.btShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "http://andtechnology.mobi/");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
        return view;
    }


}

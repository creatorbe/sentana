package com.creatorbe.sentana.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.creatorbe.sentana.MainActivity;

import java.util.HashMap;

/**
 * Created by Sandy on 7/6/2015.
 */
public class SentanaFragment extends Fragment {
    protected MainActivity activity;
    protected View pb;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity= (MainActivity) activity;
    }

    protected HashMap<Integer,Boolean> indexLoad=new HashMap<>();

    protected void addLoad(int addIndexLoad){
        pb.setVisibility(View.VISIBLE);
        indexLoad.put(addIndexLoad,true);
//        return indexLoad.size()-1;
    }

    protected void removeLoad(int removeIndexLoad){
        indexLoad.remove(removeIndexLoad);
        if(indexLoad.size()==0){
            pb.setVisibility(View.GONE);
        }
    }
}

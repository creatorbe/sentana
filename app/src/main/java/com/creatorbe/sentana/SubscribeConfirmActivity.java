package com.creatorbe.sentana;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


public class SubscribeConfirmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe_confirm);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.red));
        }
        String packages = getIntent().getStringExtra("type_name");

        ((TextView)findViewById(R.id.tvPackage)).setText(packages);
        ((TextView)findViewById(R.id.tvPrice)).setText(getIntent().getStringExtra("price"));

//        Calendar date = Calendar.getInstance();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, d/M/yyyy", Locale.getDefault());
//        String period=dateFormat.format(date.getTime()) + " - ";
//        switch (packages){
//            case R.string.seven_days_subscribe:
//                date.add(Calendar.DAY_OF_MONTH,7);
//                break;
//            case R.string.one_month_subscribe:
//                date.add(Calendar.MONTH,1);
//                break;
//            case R.string.three_months_subscribe:
//                date.add(Calendar.MONTH,3);
//                break;
//            case R.string.one_year_subscribe:
//                date.add(Calendar.YEAR,1);
//                break;
//        }
//        period+=dateFormat.format(date.getTime());
//        ((TextView)findViewById(R.id.tvPeriod)).setText(period);
    }

}

package com.creatorbe.sentana;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Sandy on 7/7/2015.
 */
public class SentanaActivity extends AppCompatActivity {
    protected boolean checkEmpty(TextView... views){
        boolean result =true;
        for(TextView view:views){
            if(view.getText().toString().length()==0){
                addError(view, getString(R.string.this_should_not_empty));
                result=false;
            }
        }
        return result;
    }
    protected void addError(TextView view,String error){
        CharSequence err = view.getError();
        if(err!=null) {
            if (err.length() > 0) err = err + "\n";
        }else{
            err="";
        }
        view.setError(err+error);
    }
}

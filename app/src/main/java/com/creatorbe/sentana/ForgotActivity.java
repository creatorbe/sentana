package com.creatorbe.sentana;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.network.APITask;

import org.json.JSONException;
import org.json.JSONObject;


public class ForgotActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_forgot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void send(View view) {


        new APITask(this, API.forgotPassword(this,((EditText)findViewById(R.id.etEmail)).getText().toString())){
            ProgressDialog pd;
            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                pd.dismiss();
                super.onPostExecute(jsonObject);

            }

            @Override
            protected void onPreExecute() {
                pd=new ProgressDialog(ForgotActivity.this);
                pd.setMessage(getString(R.string.please_wait));
                pd.setCancelable(false);
                pd.show();
                super.onPreExecute();
            }

            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {

            }
        }.execute();
    }
}

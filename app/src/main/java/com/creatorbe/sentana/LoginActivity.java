package com.creatorbe.sentana;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.network.APITask;
import com.creatorbe.sentana.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class LoginActivity extends SentanaActivity {

    private EditText etUser,etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUser=(EditText)findViewById(R.id.etUser);
        etPassword=(EditText)findViewById(R.id.etPassword);
    }

    public void forgotPassword(View view){
        startActivity(new Intent(LoginActivity.this,ForgotActivity.class));
    }

    public void register(View view) {
        startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
    }

    public void login(View view) {
        etUser.setError(null);
        etPassword.setError(null);
        if(checkEmpty(etUser,etPassword)){
            new APITask(this, API.login(etUser.getText().toString(), etPassword.getText().toString())) {
                @Override
                protected void onSuccess(JSONObject jsonObject) throws JSONException {
//                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
//                    finish();
                    HashMap<String, String> values= new HashMap<>();
                    /*
                    {"data":[{"user_id":"-Nj1LEnbis9K0tzvRMEOGLGzp5junHvmhfED50-avKU","userlevel_id":"3","user_created":"2015-06-24 07:13:55","user_update":null,"user_fullname":"testing nomor 25","username":"testing25","user_email":"testing25@yahoo.com","user_status":"unsubscribed","new_edition_notif":"on","promo_notif":"on","security_token":"EA9XvzD1FyRxM7qLNYW","subscription_id":null}]}
                     */
                    JSONObject object = jsonObject.getJSONArray("data").getJSONObject(0);
                    values.put(API.USER_ID,object.getString(API.USER_ID));
                    values.put("userlevel_id",object.getString("userlevel_id"));
                    values.put("user_created",object.getString("user_created"));
                    values.put("user_update",object.getString("user_update"));
                    values.put("phone",object.getString("phone"));
                    values.put("username",object.getString("username"));
                    values.put("user_email",object.getString("user_email"));
                    values.put("user_status",object.getString("user_status"));
                    values.put("new_edition_notif",object.getString("new_edition_notif"));
                    values.put("promo_notif",object.getString("promo_notif"));
                    values.put("security_token", object.getString("security_token"));
                    if(object.has("subscription_id"))values.put("subscription_id",object.getString("subscription_id"));
                    if(jsonObject.has("subscription_info")){
                        values.put("subscribed_from", jsonObject.getJSONObject("subscription_info").getString("subscribed_from"));
                        values.put("subscribed_to", jsonObject.getJSONObject("subscription_info").getString("subscribed_to"));
                    }
                    Preferences.puts(LoginActivity.this, Preferences.PROFILE, values);
                    setResult(RESULT_OK);
                    finish();
                }
            }.execute();
        }

    }
}

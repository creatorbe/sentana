package com.creatorbe.sentana.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBAdapter {

    public int update(String table, ContentValues values, String whereClause, String... whereArgs) {
        return mDb.update(table, values, whereClause, whereArgs);
    }
    public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return mDb.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
    }

    public long insert(String table, String nullColumnHack, ContentValues values) {
        return mDb.insert(table, nullColumnHack, values);
    }

    public int delete(String table, String whereClause, String... whereArgs) {
        return mDb.delete(table,whereClause,whereArgs);
    }


    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {

            super(context, DATABASE_NAME, null, DATABASE_VERSION);
//            this.context=context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE article(_id integer primary key,article_id text, title text,category_id text,category_name text,keywords text,preview text,news_date text)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS article");
            onCreate(db);
        }
    }




    private static final String DATABASE_NAME = "localdata.db";

    private static final int DATABASE_VERSION = 3;
    public static DBAdapter dbadapter;

    public static DBAdapter getInstance(Context context){
        if(dbadapter==null){
            dbadapter=new DBAdapter(context);
        }
        return dbadapter;
    }
    private final Context mContext;

    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public DBAdapter(Context context) {
        this.mContext = context;
    }


    public void close() {
        mDbHelper.close();
    }


    public Context getContext() {
        return mContext;
    }


    public DBAdapter open(){
        mDbHelper = new DatabaseHelper(mContext);
        if(mDb!=null)
            if(mDb.isOpen())
                return this;
        mDb = mDbHelper.getWritableDatabase();

        return this;

    }



}

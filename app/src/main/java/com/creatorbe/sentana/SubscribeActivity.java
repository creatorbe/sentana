package com.creatorbe.sentana;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.network.APITask;
import com.creatorbe.sentana.network.Network;
import com.creatorbe.sentana.util.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;


public class SubscribeActivity extends AppCompatActivity {

    private static final int REQUEST_CONFIRM = 1;
    private CheckBox cbAgree;
    private RadioGroup rg;
    private Button btSubscribe;
    private JSONArray subscribes;
    private View pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String jsonString = Preferences.get(this,Preferences.SETTING,"subscribes");
        subscribes = new JSONArray();
        if(jsonString!=null){
            try {
                subscribes = new JSONArray(jsonString);
            } catch (JSONException e) {
                Sentana.handleError(this,e);
            }
        }

        setContentView(R.layout.activity_subscribe);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.red));
        }

        rg = (RadioGroup) findViewById(R.id.rgPackage);
        pb = findViewById(R.id.progressBar);
        fillRadio();
        new AsyncTask<Void,Void,JSONArray>() {

            @Override
            protected JSONArray doInBackground(Void... params) {
                try {
                    return new JSONArray(Network.doPost(API.URL, API.subcribesList(SubscribeActivity.this)));
                } catch (Exception e) {
                    Sentana.handleError(SubscribeActivity.this,e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONArray jsonArray) {
                super.onPostExecute(jsonArray);

                pb.setVisibility(View.GONE);
                if(!subscribes.toString().equals(jsonArray.toString())) {
                    subscribes = jsonArray;
                    fillRadio();
                }

            }
        }.execute();

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkValid();
            }
        });
        cbAgree = (CheckBox) findViewById(R.id.cbAgree);
        cbAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkValid();
            }
        });
        btSubscribe = (Button) findViewById(R.id.btSubscribe);
        btSubscribe.setEnabled(false);
        btSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    final JSONObject packages = subscribes.getJSONObject(rg.getCheckedRadioButtonId());
                    new APITask(SubscribeActivity.this, API.subscribe(SubscribeActivity.this, packages.getString("type_id"))) {
                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            pb.setVisibility(View.VISIBLE);
                        }

                        @Override
                        protected void onError(Exception e) {
                            super.onError(e);
                            pb.setVisibility(View.GONE);
                        }

                        @Override
                        protected void onSuccess(final JSONObject jsonObject) throws JSONException {
                            pb.setVisibility(View.GONE);
                            HashMap<String, String> values=new HashMap<>();
                            values.put("subscription_id", jsonObject.getString("subscription_id"));
                            Preferences.puts(SubscribeActivity.this, Preferences.PROFILE, values);
                            Intent intent = new Intent(SubscribeActivity.this, SubscribeConfirmActivity.class);
                            try {
                                intent.putExtra("type_name", packages.getString("type_name"));
                                intent.putExtra("type_id", packages.getString("type_id"));
                                intent.putExtra("price",addCurrency(jsonObject.getLong("transfer_amount")));
                                startActivityForResult(intent, REQUEST_CONFIRM);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            new AlertDialog.Builder(SubscribeActivity.this)
//                                    .setMessage(jsonObject.getString("msg"))
//                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//
//                                        }
//                                    })
//                                    .create().show();
                        }
                    }.execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void fillRadio() {
        rg.removeAllViews();
        for(int i =0;i< subscribes.length();i++) {
            try {
                rg.addView(new PackageRadioButton(this, i, subscribes.getJSONObject(i).getString("type_name").toUpperCase(), subscribes.getJSONObject(i).getLong("price")));
            } catch (JSONException e) {
                Sentana.handleError(this,e);
            }
        }
    }

    private void checkValid() {
        if(rg.getCheckedRadioButtonId()>=0 && cbAgree.isChecked())
            btSubscribe.setEnabled(true);
        else
            btSubscribe.setEnabled(false);
    }

    private class PackageRadioButton extends RadioButton{

        public PackageRadioButton(Context context,int id,String title,long price) {
            super(context);
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,8,getResources().getDisplayMetrics());
            params.setMargins(0,margin,0,margin);
            setLayoutParams(params);
            setPadding(margin,margin,margin,margin);
            setGravity(Gravity.CENTER_HORIZONTAL);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
//                setAllCaps(true);
            setText(Html.fromHtml("<b>" + title + "</b><br>" + addCurrency(price)));
            setBackgroundResource(R.drawable.bg_radio_package);
            setButtonDrawable(null);
            setId(id);

            setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        }
    }

    private String addCurrency(long number){
        return String.format(new Locale("in"),"Rp. %,d,-",number);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setResult(RESULT_OK);
        finish();
    }
}

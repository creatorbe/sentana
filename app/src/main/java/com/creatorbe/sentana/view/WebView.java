package com.creatorbe.sentana.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Created by Sandy on 5/11/2016.
 */
public class WebView extends android.webkit.WebView {
    private FloatingActionButton fab;
    private Handler handler;
    private Runnable runnable;

    public WebView(Context context) {
        super(context);
        init();
    }

    public WebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public WebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public WebView(Context context, AttributeSet attrs, int defStyleAttr, boolean privateBrowsing) {
        super(context, attrs, defStyleAttr, privateBrowsing);
        init();
    }

    private void init() {
//        setOnTouchListener(new OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Log.d("TOuch",String.valueOf(event.getAction()));
//                if(event.getAction()==MotionEvent.ACTION_UP){
//                    fab.hide();
//                }
//                return false;
//            }
//        });
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                fab.hide();
            }
        };

    }


    public void setFab(FloatingActionButton fab){
        this.fab = fab;
        fab.hide();
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        Log.d("scroll", String.valueOf(t));
//        int dyConsumed = t-oldt;
//        if (fab.getVisibility() == View.VISIBLE && dyConsumed > 0) {
//            fab.hide();
//        } else if (fab.getVisibility() == View.GONE && dyConsumed < 0) {
        fab.show();
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable,2000);
//        }
    }

}

package com.creatorbe.sentana;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.creatorbe.sentana.barcodereader.BarcodeCaptureActivity;
import com.creatorbe.sentana.database.DBAdapter;
import com.creatorbe.sentana.fragment.ArticleFragment;
import com.creatorbe.sentana.fragment.FavoriteFragment;
import com.creatorbe.sentana.fragment.MainFragment;
import com.creatorbe.sentana.fragment.PagesFragment;
import com.creatorbe.sentana.fragment.ProfileFragment;
import com.creatorbe.sentana.fragment.SearchFragment;
import com.creatorbe.sentana.fragment.ShareFragment;
import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.network.APITask;
import com.creatorbe.sentana.util.Preferences;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity implements MainFragment.MainFragmentListener,
        PagesFragment.PreviewArticleListener, FavoriteFragment.FavoriteFragmentListener, ProfileFragment.ProfileListener {

    /*
    api list edition..
Bisa diakses dgn panggil class newspaper , action nya editions tanpa perlu user id & token
     */
    private static final int REQUEST_LOGIN = 0;
    private static final int REQUEST_SUBSCRIBE = 1;
    private static final int REQUEST_LOGIN_THEN_SUBSCRIBE = 2;
    private static final int REQUEST_BARCODE = 3;
    private DrawerLayout mDrawerLayout;
    private View vFavorite, vProfile, vShare, vList;
    private EditText etSearch;
    private FragmentManager fragmentManager;
    private TextView tvTime;
    private JSONArray timeRange = new JSONArray();
    private TextView tvEdition;
    private JSONArray editions = new JSONArray();
    //private boolean isSearch=false;
    private ProfileFragment profileFragment;
    private View flSubscribe, btSubscribe;
    private TextView tvSubscribe;
    private AdView mAdView;
    //    private ListView mDrawerList;
    private InterstitialAd interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        flSubscribe = findViewById(R.id.flSubscribe);
        btSubscribe = findViewById(R.id.btSubscribe);
        tvSubscribe = (TextView) findViewById(R.id.tvSubscribe);
        ImageLoaderConfiguration config = Sentana.getImageLoaderConfig(this);
        ImageLoader.getInstance().init(config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //noinspection deprecation
            getWindow().setStatusBarColor(getResources().getColor(R.color.red));
        }
        if (Preferences.get(this, Preferences.PROFILE, "subscribed_to") != null) {
            flSubscribe.setVisibility(View.GONE);
        } else if (Preferences.get(this, Preferences.PROFILE, "subscription_id") != null) {
            flSubscribe.setVisibility(View.VISIBLE);
            if (Preferences.get(this, Preferences.PROFILE, "user_status") != null) {
                tvSubscribe.setVisibility(View.VISIBLE);
                String status = Preferences.get(this, Preferences.PROFILE, "user_status");
                if (status.equals("pending_approval")) {
                    tvSubscribe.setText("Pending Approval");
                } else if (status.equals("pending_payment")) {
                    tvSubscribe.setText("Pending Payment");
                }
                btSubscribe.setVisibility(View.GONE);
            }

        }

        AdView adView = new AdView(this);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        // TODO: Add adView to your view hierarchy.
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId("ca-app-pub-2554754057880816/7044783406");
        loadinterstitial();

        // Listener for Ad
        interstitial.setAdListener(new AdListener()
        {
            // When Closed Ad, Load new Ad
            @Override
            public void onAdClosed()
            {
                super.onAdClosed();
                loadinterstitial();
            }
        });

        //mPlanetTitles = getResources().getStringArray(R.array.planets_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                etSearch.clearFocus();
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });
//        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
//        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
//                android.R.layout.simple_list_item_1, mPlanetTitles));
        // Set the list's click listener
        //mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        etSearch = (EditText) findViewById(R.id.etSearch);
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    if (!mDrawerLayout.isDrawerOpen(GravityCompat.START))
                        mDrawerLayout.openDrawer(GravityCompat.START);
                } else {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                }
                etSearch.setActivated(hasFocus);
            }
        });
        vList = findViewById(R.id.btPapers);
        vProfile = findViewById(R.id.btProfile);
        vShare = findViewById(R.id.btShare);
        vFavorite = findViewById(R.id.btFavorite);

        vProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Preferences.get(MainActivity.this, Preferences.PROFILE, "username").equalsIgnoreCase("adminmikhael")) {
                    startActivityForResult(new Intent(MainActivity.this, LoginActivity.class), REQUEST_LOGIN);
                } else {
                    if (Preferences.get(MainActivity.this, Preferences.PROFILE, API.USER_ID) != null) {
                        setActive(vProfile.getId());
                        profileFragment = new ProfileFragment();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, profileFragment).commit();
                    } else
                        startActivityForResult(new Intent(MainActivity.this, LoginActivity.class), REQUEST_LOGIN);
                }
            }
        });
        vShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setActive(vShare.getId());
                fragmentManager.beginTransaction().replace(R.id.content_frame, new ShareFragment()).commit();
            }
        });
        vFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Preferences.get(MainActivity.this, Preferences.PROFILE, API.USER_ID) != null) {
                    setActive(vFavorite.getId());
                    fragmentManager.beginTransaction().replace(R.id.content_frame, new FavoriteFragment()).commit();
                } else
                    startActivityForResult(new Intent(MainActivity.this, LoginActivity.class), REQUEST_LOGIN);

            }
        });
        vList.setActivated(true);
        fillTimeAndEditions();


        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
    }

    private void fillTimeAndEditions() {
        tvTime = (TextView) findViewById(R.id.tvTime);
        String jsonString = Preferences.get(this, Preferences.SETTING, "time_range");
        if (jsonString != null) {
            try {
                timeRange = new JSONArray(jsonString);
                if (timeRange.length() > 0) {

                    JSONObject obj = timeRange.getJSONObject(0);
                    tvTime.setTag(obj.getString("range_id"));
                    tvTime.setText(obj.getString("range_name"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeAdapter timeAdapter = new TimeAdapter();
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.time_range)
                        .setAdapter(timeAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    JSONObject obj = timeRange.getJSONObject(which);
                                    tvTime.setTag(obj.getString("range_id"));
                                    tvTime.setText(obj.getString("range_name"));
                                } catch (JSONException e) {
                                    Sentana.handleError(MainActivity.this, e);
                                }

                            }
                        })
                        .create().show();
            }
        });

        tvEdition = (TextView) findViewById(R.id.tvEdition);
        jsonString = Preferences.get(this, Preferences.SETTING, "editions");
        if (jsonString != null) {
            try {
                editions = new JSONArray(jsonString);
                if (editions.length() > 0) {

                    JSONObject obj = editions.getJSONObject(0);
                    tvEdition.setTag(obj.getString("edition_id"));
                    tvEdition.setText(obj.getString("edition_name"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        tvEdition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditionAdapter editionAdapter = new EditionAdapter();
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.time_range)
                        .setAdapter(editionAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    JSONObject obj = editions.getJSONObject(which);
                                    tvEdition.setTag(obj.getString("edition_id"));
                                    tvEdition.setText(obj.getString("edition_name"));
                                } catch (JSONException e) {
                                    Sentana.handleError(MainActivity.this, e);
                                }

                            }
                        })
                        .create().show();
            }
        });
    }

    public void search(View view) {

        setActive(etSearch.getId());
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString("word_search", etSearch.getText().toString());
        args.putString("edition_range", tvEdition.getTag().toString());
        args.putString("time_range", tvTime.getTag().toString());
        args.putString("edition", tvEdition.getText().toString());
        args.putString("time", tvTime.getText().toString());
        fragment.setArguments(args);
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    public void showList(View view) {
        setActive(vList.getId());
        MainFragment fragment = new MainFragment();
//        if(isSearch){
//            isSearch=false;
//            Bundle args=new Bundle();
//            args.putString("word_search",etSearch.getText().toString());
//            args.putString("edition",tvEdition.getText().toString());
//            args.putString("time",tvTime.getText().toString());
//            args.putString("edition_range",tvEdition.getTag().toString());
//            args.putString("time_range",tvTime.getTag().toString());
//            fragment.setArguments(args);
//        }else{
//            etSearch.setText("");
//        }
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    public void onLogout() {
        DBAdapter dbAdapter = DBAdapter.getInstance(this);
        dbAdapter.open();
        dbAdapter.delete("article", null);
        dbAdapter.close();
        SharedPreferences.Editor edit = Preferences.get(this, Preferences.PROFILE).edit();
        edit.clear();
        edit.apply();
        flSubscribe.setVisibility(View.VISIBLE);
        fragmentManager.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
        setActive(vList.getId());

        new APITask(this, API.login("adminmikhael", "adminsinaga")) {
            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
                HashMap<String, String> values= new HashMap<>();
                JSONObject object = jsonObject.getJSONArray("data").getJSONObject(0);
                values.put(API.USER_ID,object.getString(API.USER_ID));
                values.put("userlevel_id",object.getString("userlevel_id"));
                values.put("user_created",object.getString("user_created"));
                values.put("user_update",object.getString("user_update"));
                values.put("phone",object.getString("phone"));
                values.put("username",object.getString("username"));
                values.put("user_email",object.getString("user_email"));
                values.put("user_status",object.getString("user_status"));
                values.put("new_edition_notif",object.getString("new_edition_notif"));
                values.put("promo_notif",object.getString("promo_notif"));
                values.put("security_token", object.getString("security_token"));
                if(object.has("subscription_id"))values.put("subscription_id",object.getString("subscription_id"));
                if(jsonObject.has("subscription_info")){
                    values.put("subscribed_from", jsonObject.getJSONObject("subscription_info").getString("subscribed_from"));
                    values.put("subscribed_to", jsonObject.getJSONObject("subscription_info").getString("subscribed_to"));
                }
                Preferences.puts(MainActivity.this, Preferences.PROFILE, values);
                setResult(RESULT_OK);
                Intent i = new Intent(MainActivity.this, MainActivity.class);
                i.setFlags(i.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK | i.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

                finish();
            }

            @Override
            protected void onError(Exception e) {
                super.onError(e);
                Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                finish();
            }
        }.execute();
    }

    public void home(View view) {
        showList(view);
    }

    public void showScan(View view) {
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.setOrientationLocked(false);

        intentIntegrator.setCaptureActivity(CustomCaptureActivity.class);
//        intentIntegrator.initiateScan();

//        Intent intent = new Intent(getActivity(), BarcodeCaptureActivity.class);
//        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
//        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
        startActivityForResult(intentIntegrator.createScanIntent(), IntentIntegrator.REQUEST_CODE);
    }

    private class TimeAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return timeRange.length();
        }

        @Override
        public Object getItem(int position) {
            try {
                return timeRange.get(position);
            } catch (JSONException e) {
                Sentana.handleError(MainActivity.this, e);
                return null;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(MainActivity.this).inflate(android.R.layout.simple_list_item_1, parent, false);
            }
            try {
                ((TextView) convertView.findViewById(android.R.id.text1)).setText(timeRange.getJSONObject(position).getString("range_name"));
            } catch (JSONException e) {
                Sentana.handleError(MainActivity.this, e);
            }
            return convertView;
        }
    }

    private class EditionAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return editions.length();
        }

        @Override
        public Object getItem(int position) {
            try {
                return editions.get(position);
            } catch (JSONException e) {
                Sentana.handleError(MainActivity.this, e);
                return null;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(MainActivity.this).inflate(android.R.layout.simple_list_item_1, parent, false);
            }
            try {
                ((TextView) convertView.findViewById(android.R.id.text1)).setText(editions.getJSONObject(position).getString("edition_name"));
            } catch (JSONException e) {
                Sentana.handleError(MainActivity.this, e);
            }
            return convertView;
        }
    }

    public void setActive(int id) {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
            mDrawerLayout.closeDrawer(GravityCompat.START);
        etSearch.setActivated(false);
        vProfile.setActivated(vProfile.getId() == id);
        vShare.setActivated(vShare.getId() == id);
        vFavorite.setActivated(vFavorite.getId() == id);
        vList.setActivated(vList.getId() == id);
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void subscribe(View view) {

        if (Preferences.get(MainActivity.this, Preferences.PROFILE, API.USER_ID) != null)
            startActivityForResult(new Intent(MainActivity.this, SubscribeActivity.class), REQUEST_SUBSCRIBE);
        else
            startActivityForResult(new Intent(MainActivity.this, LoginActivity.class), REQUEST_LOGIN_THEN_SUBSCRIBE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null && result.getContents() != null) {
            new MainAPITask(API.qrCode(this, result.getContents()), MainActivity.this) {
                ProgressDialog progressDialog;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog = new ProgressDialog(MainActivity.this);
                    progressDialog.setMessage(getString(R.string.please_wait));
                    progressDialog.show();
                }

                @Override
                protected void onFailed(JSONObject jsonObject) throws JSONException {
                    super.onFailed(jsonObject);
                    progressDialog.dismiss();
                }

                @Override
                protected void onError(Exception e) {
                    super.onError(e);
                    progressDialog.dismiss();
                }

                @Override
                protected void onSuccess(JSONObject jsonObject) throws JSONException {
                    progressDialog.dismiss();
                    new AlertDialog.Builder(MainActivity.this).setMessage(jsonObject.getString("msg")).create().show();

                }
            }.execute();
            return;
        }
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_LOGIN:
                case REQUEST_SUBSCRIBE:
                    setActive(vList.getId());
                    fragmentManager.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
                    break;
                case REQUEST_LOGIN_THEN_SUBSCRIBE:
                    startActivityForResult(new Intent(MainActivity.this, SubscribeActivity.class), REQUEST_SUBSCRIBE);
                    break;
                case REQUEST_BARCODE:
                    final Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    new MainAPITask(API.qrCode(this, barcode.displayValue), MainActivity.this) {
                        ProgressDialog progressDialog;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            progressDialog = new ProgressDialog(MainActivity.this);
                            progressDialog.setMessage(getString(R.string.please_wait));
                            progressDialog.show();
                        }

                        @Override
                        protected void onFailed(JSONObject jsonObject) throws JSONException {
                            super.onFailed(jsonObject);
                            progressDialog.dismiss();
                        }

                        @Override
                        protected void onError(Exception e) {
                            super.onError(e);
                            progressDialog.dismiss();
                        }

                        @Override
                        protected void onSuccess(JSONObject jsonObject) throws JSONException {
                            progressDialog.dismiss();
                            new AlertDialog.Builder(MainActivity.this).setMessage(jsonObject.getString("msg")).create().show();

                        }
                    }.execute();
                    break;
            }


        }

    }

    @Override
    public void onPaperClick(String news_id) {
        PagesFragment fragment = new PagesFragment();
        Bundle args = new Bundle();
        args.putString("newspaper_id", news_id);
        fragment.setArguments(args);
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onSeeArticle(String article_id, String title) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();
        args.putString("article_id", article_id);
        args.putString("title", title);
        fragment.setArguments(args);
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();

    }

    @Override
    public void onFavClick(String article_id, String title) {

        onSeeArticle(article_id, title);

    }

    public static abstract class MainAPITask extends APITask {


        private final MainActivity activity;

        public MainAPITask(HashMap<String, String> postParams, HashMap<String, String> files, MainActivity activity) {
            super(activity, postParams, files);
            this.activity = activity;
        }

        public MainAPITask(HashMap<String, String> postParams, MainActivity activity) {
            this(postParams, null, activity);

        }

        @Override
        protected void onPendingApproval(JSONObject jsonObject) {
            super.onPendingApproval(jsonObject);
            activity.flSubscribe.setVisibility(View.VISIBLE);
            activity.btSubscribe.setVisibility(View.GONE);
            activity.tvSubscribe.setVisibility(View.VISIBLE);
            activity.tvSubscribe.setText(R.string.pending_approval);

        }

        @Override
        protected void onPendingPayment(JSONObject jsonObject) {
            super.onPendingPayment(jsonObject);
            activity.flSubscribe.setVisibility(View.VISIBLE);
            activity.btSubscribe.setVisibility(View.GONE);
            activity.tvSubscribe.setVisibility(View.VISIBLE);
            activity.tvSubscribe.setText(R.string.pending_payment);
        }

        @Override
        protected void onSubscriptionAdded() {
            activity.flSubscribe.setVisibility(View.GONE);
            activity.btSubscribe.setVisibility(View.GONE);
            activity.tvSubscribe.setVisibility(View.GONE);
        }

        @Override
        protected void onSubscriptionExpired() {
            activity.flSubscribe.setVisibility(View.VISIBLE);
            activity.btSubscribe.setVisibility(View.VISIBLE);
            activity.tvSubscribe.setVisibility(View.GONE);

        }
    }

    private void loadinterstitial()
    {
        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();

        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);
    }

    // Show Interstitial Ad
    private void showInterstitialAd()
    {
        // return, if Ad data is no loaded
        if (!interstitial.isLoaded()) {
            return;
        }

        // Show Ad
        interstitial.show();
    }

    // Back button
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

//        // Make rand
//        Random rnd = new Random();
//
//        // Omikuji
//        int Omikuji = rnd.nextInt(2);
//        if (Omikuji == 0) {
//            // Go to Show Interstitial Ad
//            showInterstitialAd();
//        }

        showInterstitialAd();

    }
}

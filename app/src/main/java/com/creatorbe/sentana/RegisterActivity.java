package com.creatorbe.sentana;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.network.APITask;

import org.json.JSONException;
import org.json.JSONObject;


public class RegisterActivity extends SentanaActivity {

    private EditText etUser,etEmail,etPhone,etPassword,etConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etUser = (EditText)findViewById(R.id.etUser);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPhone = (EditText)findViewById(R.id.etPhone);
        etPassword = (EditText)findViewById(R.id.etPassword);
        etConfirm = (EditText)findViewById(R.id.etConfirm);
    }

    public void register(View view) {
        etUser.setError(null);
        etEmail.setError(null);
        etPhone.setError(null);
        etPassword.setError(null);
        etConfirm.setError(null);
        boolean pass = checkEmpty(etUser,etEmail,etPhone,etPassword,etConfirm);
        if(!etPassword.getText().toString().equals(etConfirm.getText().toString())){
            addError(etConfirm,getString(R.string.password_not_equal));
            pass=false;
        }
        if(pass) {
            /*
            class: user
action: registration
username: testing390
password: 123456
email: apaandeh.mautauaja@iseng.net
phone: 087766790765
             */
        new APITask(this, API.register(this, etUser.getText().toString()
                , etPassword.getText().toString()
                , etEmail.getText().toString()
                , etPhone.getText().toString())) {
            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
                new AlertDialog.Builder(RegisterActivity.this)
                        .setMessage(getString(R.string.registration_succesful))
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .create().show();

            }
        }.execute();
        }
    }
}

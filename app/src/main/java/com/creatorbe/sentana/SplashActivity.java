package com.creatorbe.sentana;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.creatorbe.sentana.network.API;
import com.creatorbe.sentana.network.APITask;
import com.creatorbe.sentana.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class SplashActivity extends Activity {
    private int load=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        new APITask(SplashActivity.this, API.editions(SplashActivity.this)){
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                load++;
//            }
//
//            @Override
//            protected void onPostExecute(JSONObject jsonObject) {
//                super.onPostExecute(jsonObject);
//                load--;
//                exit();
//            }
//
//            @Override
//            protected void onSuccess(JSONObject jsonObject) {
//                try{
//                    HashMap<String, String> values = new HashMap<>();
//                    values.put("editions", jsonObject.getJSONArray("data").toString());
//                    Preferences.puts(SplashActivity.this, Preferences.SETTING, values);
//                }catch (Exception e){
//                    Sentana.handleError(SplashActivity.this,e);
//                }
//            }
//        }.execute();
//
//        //time range
//        new APITask(this, API.timeRange(this)) {
//            @Override
//            protected void onPostExecute(JSONObject jsonObject) {
//                super.onPostExecute(jsonObject);
//                load--;
//                exit();
//            }
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                load++;
//            }
//
//            @Override
//            protected void onSuccess(JSONObject jsonObject) throws JSONException {
//                try{
//                    HashMap<String, String> values = new HashMap<>();
//                    values.put("time_range", jsonObject.getJSONArray("data").toString());
//                    Preferences.puts(SplashActivity.this, Preferences.SETTING, values);
//                }catch (Exception e){
//                    Sentana.handleError(SplashActivity.this,e);
//                }
//            }
//        }.execute();


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startActivity(new Intent(SplashActivity.this,MainActivity.class));
//                finish();
//            }
//        },3000);

        new APITask(this, API.login("adminmikhael", "adminsinaga")) {
            @Override
            protected void onSuccess(JSONObject jsonObject) throws JSONException {
//                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
//                    finish();
                HashMap<String, String> values= new HashMap<>();
                    /*
                    {"data":[{"user_id":"-Nj1LEnbis9K0tzvRMEOGLGzp5junHvmhfED50-avKU","userlevel_id":"3","user_created":"2015-06-24 07:13:55","user_update":null,"user_fullname":"testing nomor 25","username":"testing25","user_email":"testing25@yahoo.com","user_status":"unsubscribed","new_edition_notif":"on","promo_notif":"on","security_token":"EA9XvzD1FyRxM7qLNYW","subscription_id":null}]}
                     */
                JSONObject object = jsonObject.getJSONArray("data").getJSONObject(0);
                values.put(API.USER_ID,object.getString(API.USER_ID));
                values.put("userlevel_id",object.getString("userlevel_id"));
                values.put("user_created",object.getString("user_created"));
                values.put("user_update",object.getString("user_update"));
                values.put("phone",object.getString("phone"));
                values.put("username",object.getString("username"));
                values.put("user_email",object.getString("user_email"));
                values.put("user_status",object.getString("user_status"));
                values.put("new_edition_notif",object.getString("new_edition_notif"));
                values.put("promo_notif",object.getString("promo_notif"));
                values.put("security_token", object.getString("security_token"));
                if(object.has("subscription_id"))values.put("subscription_id",object.getString("subscription_id"));
                if(jsonObject.has("subscription_info")){
                    values.put("subscribed_from", jsonObject.getJSONObject("subscription_info").getString("subscribed_from"));
                    values.put("subscribed_to", jsonObject.getJSONObject("subscription_info").getString("subscribed_to"));
                }
                Preferences.puts(SplashActivity.this, Preferences.PROFILE, values);
                setResult(RESULT_OK);
                startActivity(new Intent(SplashActivity.this,MainActivity.class));
                finish();
            }

            @Override
            protected void onError(Exception e) {
                super.onError(e);
                Toast.makeText(SplashActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                finish();
            }
        }.execute();

    }

    private void exit() {
        if(load==0){
            startActivity(new Intent(SplashActivity.this,MainActivity.class));
                finish();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_splash, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}

package com.creatorbe.sentana.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sandy on 7/14/2015.
 */
public class Preferences {

    public static final String PROFILE = "profile";
    public static final String SETTING = "setting";

    public static void puts(Context context, String name,HashMap<String,String> values) {
        SharedPreferences.Editor edit = context.getSharedPreferences(name, Context.MODE_PRIVATE).edit();
        for (Map.Entry<String, String> pair : values.entrySet()) {
            edit.putString(pair.getKey(), pair.getValue());
        }
        edit.apply();
    }

    public static String get(Context context, String name, String key){
        return get(context,name).getString(key,null);
    }

    public static SharedPreferences get(Context context, String name) {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }
}
